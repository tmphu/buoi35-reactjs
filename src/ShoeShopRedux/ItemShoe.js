import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, CHANGE_DETAIL } from "./redux/constant/shoeConstant";

class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.itemShoe;
    return (
      <div className="col-3">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt="" />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleBuyShoe(this.props.itemShoe);
              }}
            >
              Buy
            </button>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleDetail(this.props.itemShoe);
              }}
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleBuyShoe: (value) => {
      dispatch({
        type: ADD_TO_CART,
        payload: value,
      });
    },
    handleDetail: (value) => {
      dispatch({
        type: CHANGE_DETAIL,
        payload: value,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
