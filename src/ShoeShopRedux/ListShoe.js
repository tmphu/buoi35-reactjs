import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderShoeList() {
    return this.props.list.map((item, index) => {
      return <ItemShoe key={index} itemShoe={item} />;
    });
  }
  render() {
    return <div className="row">{this.renderShoeList()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
