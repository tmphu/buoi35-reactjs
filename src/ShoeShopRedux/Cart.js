import React, { Component } from "react";
import { connect } from "react-redux";
import { UPDATE_CART } from "./redux/constant/shoeConstant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              className="btn btn-danger mr-1"
              onClick={() => {
                this.props.updateCart(index, "DECREASE");
              }}
            >
              -
            </button>
            {item.number}
            <button
              className="btn btn-success ml-1"
              onClick={() => {
                this.props.updateCart(index, "INCREASE");
              }}
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    updateCart: (index, adjustment) => {
      dispatch({
        type: UPDATE_CART,
        payload: { index, adjustment },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
