import { dataShoe } from "../../dataShoe.js";
import {
  ADD_TO_CART,
  CHANGE_DETAIL,
  UPDATE_CART,
} from "../constant/shoeConstant.js";

let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[0],
  cart: [],
};

export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    case CHANGE_DETAIL:
      state.detail = action.payload;
      return { ...state };
    case UPDATE_CART: {
      let cloneCart = [...state.cart];
      let index = action.payload.index;
      let adjustment = action.payload.adjustment;
      if (adjustment === "INCREASE") {
        cloneCart[index].number++;
      }
      if (adjustment === "DECREASE") {
        cloneCart[index].number--;
        if (cloneCart[index].number === 0) {
          cloneCart.splice(index, 1);
        }
      }
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
